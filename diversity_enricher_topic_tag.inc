<?php
/**
 * @file
 * This file contains all tag and topic-related methods.
 */

/**
 * Function diversity_enricher_load_topic_trees.
 *
 * Returns rendered tree of articles that are annotated by specific topic 
 * including other article tags (thought to be called by ajax function).
 *
 * @param string $topic
 *   Topic for which article tree should be created.
 * @param string $type
 *   The type of delivery (defaults to ajax).
 *
 * @return string
 *   Rendered tree of articles with corresponding topics.
 */
function diversity_enricher_load_topic_trees($topic, $type = 'ajax') {

  $ret = array();

  $prop_arr = diversity_enricher_retrieve_settings();

  $server = $prop_arr['OWLIM_SERVER'];
  $rep = $prop_arr['OWLIM_REP'];

  /* Query with limited amount of articles (to limit computation time) with
   * requested tag.
   */
  $query = 'select distinct ?s ?polarity ?sentimentScore where {?s sioc:topic ?o. ?o sioc:content "' . $topic . '" . ?s kdo:hasSentiment ?sentiment .
        ?sentiment kdo:hasScore ?sentimentScore .
        ?sentiment kdo:hasPolarity ?polarity .
      } limit 40';

  // Running query.
  $result = diversity_enricher_query_owlim_kdo($server, $rep, $query);

  $positive_results = array();
  $neutral_results = array();
  $negative_results = array();

  // Walking through result set.
  foreach ($result as $related_row) {

    $related_uri = $related_row['s'];
    $related_polarity = $related_row['polarity'];

    // Get node ids of related articles.
    $related_node_id = diversity_enricher_get_drupal_owlim_mapping_by_uri($related_uri);
    if ($related_node_id != NULL) {
      $new_article = array();
      $topics = diversity_enricher_get_article_topics_by_uri($related_uri, $server, $rep);
      $new_article['node_id'] = $related_node_id;
      $new_article['topics'] = diversity_enricher_copy_column($topics, 'content');

      // Move article to corresponding sentiment tree.
      if (diversity_enricher_check_positive_polarity($related_polarity)) {
        $positive_results[] = $new_article;
      }
      elseif (diversity_enricher_check_neutral_polarity($related_polarity)) {
        $neutral_results[] = $new_article;
      }
      elseif (diversity_enricher_check_negative_polarity($related_polarity)) {
        $negative_results[] = $new_article;
      }
    }
  }

  // Remove redundant instances.
  $positive_results = diversity_enricher_make_result_array_unique($positive_results);
  $neutral_results = diversity_enricher_make_result_array_unique($neutral_results);
  $negative_results = diversity_enricher_make_result_array_unique($negative_results);

  $ret = '<h2>Articles about the topic "' . $topic . '":<h2>' .
         '<div class="cuttingDiv intextDiv">Positive Articles<div class="diversityEnricherTopicTreeList">' . diversity_enricher_construct_tree($positive_results) . '</div></div>' .
         '<div class="cuttingDiv intextDiv">Neutral Articles<div class="diversityEnricherTopicTreeList">' . diversity_enricher_construct_tree($neutral_results) . '  </div></div>' .
         '<div class="cuttingDiv intextDiv">Negative Articles<div class="diversityEnricherTopicTreeList">' . diversity_enricher_construct_tree($negative_results) . '</div></div>';

  $commands = array();
  $commands[] = ajax_command_html('#diversityEnricherIntextAnnotationPopup', $ret);
  $commands[] = ajax_command_invoke('#diversityEnricherIntextAnnotationPopup', 'modal');
  $output = array('#type' => 'ajax', '#commands' => $commands);
  ajax_deliver($output);
}

/**
 * Function diversity_enricher_load_tag_trees.
 *
 * Returns rendered tree of articles that are annotated by specific tag
 * including other article tags (thought to be called by ajax function).
 *
 * @param string $tag
 *   Tag for which article tree should be created.
 * @param string $type
 *   The type of delivery (defaults to ajax).
 *
 * @return string
 *   Rendered tree of articles with corresponding tags.
 */
function diversity_enricher_load_tag_trees($tag, $type = 'ajax') {

  $ret = array();
  $prop_arr = diversity_enricher_retrieve_settings();

  // Get requested tag.
  $tag = htmlspecialchars_decode($tag);
  $server = $prop_arr['OWLIM_SERVER'];
  $rep = $prop_arr['OWLIM_REP'];

  /* Query with limited amount of articles (to limit computation time) with
   * requested tag.
   */
  $query = 'select distinct ?s ?polarity ?sentimentScore where {?s sioc:tag "' . $tag . '" . ?s kdo:hasSentiment ?sentiment .
        ?sentiment kdo:hasScore ?sentimentScore .
        ?sentiment kdo:hasPolarity ?polarity .
      } limit 40';

  // Running query.
  $result = diversity_enricher_query_owlim_kdo($server, $rep, $query);

  $positive_results = array();
  $neutral_results = array();
  $negative_results = array();

  // Walking through resultset.
  foreach ($result as $related_row) {

    $related_uri = $related_row['s'];
    $related_polarity = $related_row['polarity'];

    // Get node ids of related articles.
    $related_node_id = diversity_enricher_get_drupal_owlim_mapping_by_uri($related_uri);

    if ($related_node_id != NULL) {

      $new_article = array();

      // Get all tags for related article.
      $tags = diversity_enricher_get_article_tags_by_uri($related_uri, $server, $rep);
      $new_article['node_id'] = $related_node_id;
      $new_article['topics'] = diversity_enricher_copy_column($tags, 'tag');

      // Move article to corresponding sentiment tree.
      if (diversity_enricher_check_positive_polarity($related_polarity)) {
        $positive_results[] = $new_article;
      }
      elseif (diversity_enricher_check_neutral_polarity($related_polarity)) {
        $neutral_results[] = $new_article;
      }
      elseif (diversity_enricher_check_negative_polarity($related_polarity)) {
        $negative_results[] = $new_article;
      }
    }

  }

  // Remove redundant instances.
  $positive_results = diversity_enricher_make_result_array_unique($positive_results);
  $neutral_results = diversity_enricher_make_result_array_unique($neutral_results);
  $negative_results = diversity_enricher_make_result_array_unique($negative_results);

  $ret = '<h2>Articles about the tag "' . $tag . '":<h2>' .
         '<div class="cuttingDiv intextDiv">Positive Articles<div class="diversityEnricherTopicTreeList">' . diversity_enricher_construct_tree($positive_results) . '</div></div>' .
         '<div class="cuttingDiv intextDiv">Neutral Articles<div class="diversityEnricherTopicTreeList">' . diversity_enricher_construct_tree($neutral_results) . '  </div></div>' .
         '<div class="cuttingDiv intextDiv">Negative Articles<div class="diversityEnricherTopicTreeList">' . diversity_enricher_construct_tree($negative_results) . '</div></div>';

  $commands = array();
  $commands[] = ajax_command_html('#diversityEnricherIntextAnnotationPopup', $ret);
  $commands[] = ajax_command_invoke('#diversityEnricherIntextAnnotationPopup', 'modal');
  $output = array('#type' => 'ajax', '#commands' => $commands);
  ajax_deliver($output);

}
