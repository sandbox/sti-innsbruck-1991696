<?php
/**
 * @file
 * This file contains all export-related functions of the module.
 */

/**
 * Function diversity_enricher_rdf_rest_callback.
 *
 * Implements the rest menu item for rdf annotation retrieval for a specific
 * article (in rdf/xml format).
 */
function diversity_enricher_rdf_rest_callback() {

  $prop_arr = diversity_enricher_retrieve_settings();

  $dest = drupal_get_destination();
  $splitted = explode('/', $dest['destination']);
  $last_index = count($splitted) - 1;
  $last_parameter = $splitted[$last_index];

  if (is_numeric($last_parameter)) {
    $node_id = $last_parameter;
  }
  else {
    exit;
  }
  $rdf = array(
    'rdf' => diversity_enricher_build_article_rdf_by_node_id($node_id, $prop_arr['OWLIM_SERVER'], $prop_arr['OWLIM_REP'], 'rdf'),
  );
  print $rdf['rdf'];
  exit;

}

/**
 * Function diversity_enricher_turtle_rest_callback.
 *
 * Implements the rest menu item for rdf annotation retrieval for a specific
 * article (in turtle format).
 */
function diversity_enricher_turtle_rest_callback() {

  $prop_arr = diversity_enricher_retrieve_settings();

  $dest = drupal_get_destination();
  $splitted = explode('/', $dest['destination']);
  $last_index = count($splitted) - 1;
  $last_parameter = $splitted[$last_index];

  if (is_numeric($last_parameter)) {
    $node_id = $last_parameter;
  }
  else {
    exit;
  }

  $rdf = array(
    'rdf' => diversity_enricher_build_article_rdf_by_node_id($node_id, $prop_arr['OWLIM_SERVER'], $prop_arr['OWLIM_REP'], 'turtle'),
  );
  print $rdf['rdf'];
  exit;

}

/**
 * Function diversity_enricher_rdfjson_rest_callback.
 *
 * Implements the rest menu item for rdf annotation retrieval for a specific
 * article (in json format).
 */
function diversity_enricher_rdfjson_rest_callback() {

  $prop_arr = diversity_enricher_retrieve_settings();

  $dest = drupal_get_destination();
  $splitted = explode('/', $dest['destination']);
  $last_index = count($splitted) - 1;
  $last_parameter = $splitted[$last_index];

  if (is_numeric($last_parameter)) {
    $node_id = $last_parameter;
  }
  else {
    exit;
  }

  $rdf = array(
    'rdf' => diversity_enricher_build_article_rdf_by_node_id($node_id, $prop_arr['OWLIM_SERVER'], $prop_arr['OWLIM_REP'], 'rdfjson'),
  );
  print $rdf['rdf'];
  exit;

}

/**
 * Function diversity_enricher_rdf_structure_callback.
 *
 * This function defines the menu callback for the rdf structure retrieval (for
 * one specific article).
 *
 * @param int $node_id
 *   Node id of article.
 * @param string $type
 *   The type of delivery (defaults to ajax).
 */
function diversity_enricher_rdf_structure_callback($node_id, $type = 'ajax') {

  $prop_arr = diversity_enricher_retrieve_settings();

  // Create rdf for current article.
  $rdf = diversity_enricher_build_article_rdf_by_node_id($node_id, $prop_arr['OWLIM_SERVER'], $prop_arr['OWLIM_REP']);

  // Load article to memory model.
  $model = ModelFactory::getMemModel();
  $model->loadFromString($rdf, 'rdfxml');

  ob_start();

  // Create graphical output and write it to requestor.
  $model->writeAsHtmlTable();
  $ret = ob_get_contents();

  ob_end_clean();

  $commands = array();
  $commands[] = ajax_command_html('#diversityEnricherRdfStructureDialog', $ret);
  $commands[] = ajax_command_invoke('#diversityEnricherRdfStructureDialog', 'dialog', array('open'));
  $output = array('#type' => 'ajax', '#commands' => $commands);
  ajax_deliver($output);
}

/**
 * Function diversity_enricher_rest_endpoint.
 *
 * This function controls the rest endpoint of the extension where it is 
 * possible to access all article annotations by a rest interface.
 */
function diversity_enricher_rest_endpoint() {

  $ret = '';

  $dest = drupal_get_destination();
  $splitted = explode('/', $dest['destination']);
  $last_index = count($splitted) - 1;

  $last_parameter = $splitted[$last_index];
  if (is_numeric($last_parameter) && $last_index > 1 && $splitted[$last_index - 1] == 'articles') {
    $ret = diversity_enricher_retrieve_rest_item($last_parameter);
  }
  elseif ($last_parameter == 'articles') {
    $ret = diversity_enricher_retrieve_all_rest_items();
  }

  print json_encode($ret);
  exit;

}

/**
 * Function diversity_enricher_build_article_rdf_by_node_id.
 *
 * This function creates the rdf representation of a specific article annotation
 * given the drupal node id.
 *
 * @param int $node_id
 *   The node id of the article.
 * @param string $server
 *   The address of the server.
 * @param string $rep
 *   The name of the repository.
 * @param string $parse_output
 *   The output format (defaults to 'rdf').
 *
 * @return rdf
 *   Rdf formalation of article annotation.
 */
function diversity_enricher_build_article_rdf_by_node_id($node_id, $server, $rep, $parse_output = 'rdf') {

  $article_uri = diversity_enricher_get_drupal_owlim_mapping_by_node_id($node_id);
  return diversity_enricher_build_article_rdf_by_sesame_uri($article_uri, $server, $rep, NULL, NULL, $parse_output);

}

/**
 * Function diversity_enricher_retrieve_all_rest_items.
 *
 * Generates structure containg article annotation for all articles in database
 * (do not use when database is very large).
 *
 * @return array
 *   Annotation of all articles.
 */
function diversity_enricher_retrieve_all_rest_items() {

  $ret = array();

  $nids = diversity_enricher_get_node_ids_from_db();
  foreach ($nids as $nid) {
    $ret[] = diversity_enricher_retrieve_rest_item($nid['nodeid']);
  }

  return $ret;
}

/**
 * Function diversity_enricher_retrieve_rest_item.
 *
 * Generates structure that captures article annotation for specific article.
 *
 * @param int $node_id
 *   Current article id.
 *
 * @return array
 *   Article annotation.
 */
function diversity_enricher_retrieve_rest_item($node_id) {

  $ret = array();
  $prop_arr = diversity_enricher_retrieve_settings();

  $ret['platform'] = 'drupal';
  $ret['creationDate'] = diversity_enricher_get_creation_date_from_db($node_id);
  $article_uri = diversity_enricher_get_drupal_owlim_mapping_by_node_id($node_id);
  $ret['topics'] = diversity_enricher_get_article_topics_by_uri($article_uri, $prop_arr['OWLIM_SERVER'], $prop_arr['OWLIM_REP']);
  $ret['tags'] = diversity_enricher_get_article_tags_by_uri($article_uri, $prop_arr['OWLIM_SERVER'], $prop_arr['OWLIM_REP']);
  $article = diversity_enricher_get_article_information_by_uri($article_uri, $prop_arr['OWLIM_SERVER'], $prop_arr['OWLIM_REP']);
  $ret['sentiments'] = array($article[0]['polarity']);
  $ret['mentions'] = array();

  return $ret;
}
