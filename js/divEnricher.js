/**
 * @file
 * Javascript file
 */

(function ($) {

  Drupal.behaviors.diversityEnricher = {
    attach: function(context, settings) {

      activateTrees();

      $('[name=enableDiversity]').closest("form").find('[value=save]').hide();

      $('#diversityEnricherRdfStructureDialog').dialog({autoOpen: false, modal: true, height: 500, width: 1000});
      $('#diversityEnricherRdfStructureDialog').dialog('option', 'title', 'RDF Structural View');

      $('[name=enableDiversity]').click(function () {
        $(this).closest("form").find('[value=save]').click();
      });

    }
  };

  function activateTrees() {
    $(".diversityEnricherTreeList").jstree({
        "themes" : {
          "theme" : "classic",
          "dots" : false,
          "icons" : false
          },
        "plugins" : [ "themes", "ui", "html_data" ]
      }).bind("select_node.jstree", function (event, data) {
        link = data.rslt.obj.find(".hrefA").attr("href");
        if($(data.rslt.obj).hasClass('jstree-leaf')) {
          tagLabel = $(data.rslt.obj).find("a").attr("title");
          prepareTagIntextAnnotation(tagLabel);
        }
        if(link === undefined) {
        } else{
          window.location.href = link;
        }
      });
    $(".diversityEnricherTopicTreeList").jstree({
        "themes" : {
          "theme" : "classic",
          "dots" : false,
          "icons" : false
          },
        "plugins" : [ "themes", "ui", "html_data" ]
      }).bind("select_node.jstree", function (event, data) {
        link = data.rslt.obj.find(".hrefA").attr("href");
        if($(data.rslt.obj).hasClass('jstree-leaf')) {
          tagLabel = $(data.rslt.obj).find("a").attr("title");
          prepareIntextAnnotation(tagLabel);
        }
        if(link === undefined) {
        } else{
          window.location.href = link;
        }
      });
  }

})(jQuery);
