Version 1.0

REQUIRED LIBRARIES:
x to be put into the ./lib folder:
    - lib/rdfapi-php (http://sourceforge.net/projects/rdfapi-php/)
    - lib/arc2 (https://github.com/semsol/arc2)
    - lib/phpSesame (https://github.com/athalhammer/phpSesame)

x to be put into the ./js folder:
    - js/jquery.simplemodal.1.4.4.min.js (http://code.google.com/p/simplemodal/
      downloads/detail?name=simplemodal-demo-basic-1.4.4.zip)
    - js/jquery.jstree.js (download from http://www.jstree.com/,
      add jquery.jstree.js and themes to the ./js folder)

x to be put into ./css folder:
    - css/basic.css (http://code.google.com/p/simplemodal/
      downloads/detail?name=simplemodal-demo-basic-1.4.4.zip)

IMPORTANT:
x adapt the scripts [] / files[] paths in diversity_enricher.info if necessary
x adapt the RDFAPI_INCLUDE_DIR constant in diversity_enricher.module (line 12)
  if you use differnt folder names

Prerequisites:
x Installed Drupal Installation
x Installed and running instance of SESAME (http://www.openrdf.org/)
x Running instance of the RENDER Ranking Service
  (https://github.com/athalhammer/RENDER-ranking-service) (recommended!)
x Running instance of Enrycher (http://enrycher.ijs.si/)
x This program uses HTTP_Request2 of the Pear library
  (http://pear.php.net/package/HTTP_Request2/redirected). This needs to be
  installed on your server.

Installation Procedure:
x Copy the Diversity Enricher module folder to your extension folder
  (e.g. $YOUR_DRUPAL_PATH/sites/all/modules/)
x Activate the Diversity Enricher module (e.g. click on "Modules" in the admin
  interface, search "Diversity Enricher" and activate it)
x Configure Diversity Enricher (only possible with Admin or User rights)
    - Go to Configuration/DiversityEnricher: Admin in the admin interface
    - Fill out the requested fields (all of them are needed to ensure that
      Diversity Enricher is working properly)
    - Go to Structure/Blocks in the admin interface
    - Search for Diversity Enricher and place it at "Sidebar Second"
      (or anywhere else you want to have it displayed).
x Everything should work fine now

Bugs:
In case you observe bugs do not hesitate to report them either at drupal.org
(http://drupal.org/node/1991696/) or via e-mail to

simon.hangl@sti2.at,
andreas.thalhammer@sti2.at,
ioan.toma@sti2.at

Authors:
Simon Hangl (simon.hangl@sti2.at),
Andreas Thalhammer (andreas.thalhammer@sti2.at)
